#include <stdio.h>
#include <stdlib.h>

typedef struct {
    unsigned char first_byte;
    unsigned char start_chs[3];
    unsigned char partition_type;
    unsigned char end_chs[3];
    char starting_cluster[4];
    char file_size[4];
} __attribute((packed)) PartitionTable;

typedef struct {
    unsigned char jmp[3];
    char oem[8];
    unsigned short sector_size;
	unsigned char sectors_per_cluster; 
    unsigned short reserved_sectors; 
    unsigned char number_of_fats;
    unsigned short root_dir_entries; 
    unsigned short root_num_sectors; 
    unsigned char media_type; 
    unsigned short fat_size_sectors; 
    unsigned short sectors_per_track;
    unsigned short number_of_heads; 
    unsigned long number_of_sectors; 
    unsigned long number_of_sectors_fs; 
    unsigned char bios_int; 
    unsigned char not_used; 
    unsigned char ext_boot_signaturel; 
    unsigned int volume_id; 
    char volume_label[11];
    char fs_type[8];
    char boot_code[448];
    unsigned short boot_sector_signature;
} __attribute((packed)) Fat12BootSector;

typedef struct {
	unsigned char filename[8]; 
    unsigned char file_extension[3]; 
    unsigned char file_attribute; 
    unsigned char reserved; 
    unsigned char file_creation_time; 
    unsigned short file_creation_time_hours; 
    unsigned short file_creation_day; 
    unsigned short file_accessed_day;  
    unsigned short first_cluster_add; 
    unsigned short written_time; 
    unsigned short written_day; 
    unsigned short first_cluster_add_2; 
    unsigned long file_size; 
} __attribute((packed)) Fat12Entry;

unsigned int cluster_size,cluster_2_start;
unsigned short bytes_per_sector;

void print_dir_info(Fat12Entry *entry,int i) {

   switch(entry->filename[0]) {
        case 0x00:
            break; 
        case 0x0E5:            
            printf("  --Archivo borrado: [?%.7s.%.3s]\n", entry->filename, entry->file_extension);
            break;
        default:
            switch(entry->file_attribute) {
                case 0x10:
                    printf("  --Directorio: [%.8s.%.3s], Cluster de inicio:%d \n", entry->filename, entry->file_extension,entry->first_cluster_add_2);
                    show_dir_files(entry);
                    break;
                case 0x20:
                    printf("  --Archivo:%s --Entrada: #%i,Cluster de inicio:%d\n",entry->filename,i,entry->first_cluster_add_2);
                    break;        
            }  
    
    }
            
}

void show_dir_files(Fat12Entry *entry){ 
    Fat12Entry new_entry;
    FILE * in = fopen("test_punto3.img", "rb");
    unsigned int file_cluster_start=((entry->first_cluster_add_2-2)*cluster_size)+cluster_2_start;
    unsigned int file_cluster_end=file_cluster_start+cluster_size-1;
    int b;
    int index=1;

    fseek(in,file_cluster_start,SEEK_SET); 
    fread(&new_entry, sizeof(new_entry), 1, in);

     printf("  --Byte inicial: %i |Byte final: %i  \n",file_cluster_start,file_cluster_end);   

    for(b=file_cluster_start+32; b<=file_cluster_end; b+=32) {      
        fseek(in,b,SEEK_SET); 
        fread(&new_entry, sizeof(new_entry), 1, in);
        print_dir_info(&new_entry,index);
    }
    fclose(in);
}

void print_file_info(Fat12Entry *entry,int i) {

   switch(entry->filename[0]) {
        case 0x00:
            break; // unused entry
        case 0x0E5: // Completar los ...
            
            printf("Archivo borrado: [?%.7s.%.3s] --Entrada: #%i,Cluster de inicio:%d\n", entry->filename, entry->file_extension,i,entry->first_cluster_add_2);
            break;
        default:
            switch(entry->file_attribute) {
                case 0x10:
                    printf("Directorio: [%.8s.%.3s], Cluster de inicio:%d \n", entry->filename, 						entry->file_extension,entry->first_cluster_add_2);
                    show_dir_files(entry);
                    break;
                case 0x20:
                    printf("Archivo:%s --Entrada: #%i,Cluster de inicio:%d\n",entry->filename,i,entry->first_cluster_add_2);
                    break;        
        }
    
    }
              
}

int main() {
    FILE * in = fopen("test.img", "rb");
    int i;
    PartitionTable pt[4];
    Fat12BootSector bs;
    Fat12Entry entry;
   
    fseek(in,446,SEEK_SET);
    fread(pt,sizeof(PartitionTable),4,in);
    
    for(i=0; i<4; i++) {        
        if(pt[i].partition_type == 1) {
            printf("Encontrada particion FAT12 %d\n", i);
            break;
        }
    }
    
    if(i == 4) {
        printf("No encontrado filesystem FAT12, saliendo...\n");
        return -1;
    }
    
    fseek(in, 0, SEEK_SET);
	
	fread(&bs,sizeof(Fat12BootSector),1,in);

    cluster_size=bs.sectors_per_cluster*bs.sector_size;
    bytes_per_sector=bs.sector_size;
    
    printf("En  0x%X, sector size %d, FAT size %d sectors, %d FATs\n\n", 
           ftell(in), bs.sector_size, bs.fat_size_sectors, bs.number_of_fats);
           
    fseek(in, (bs.reserved_sectors-1 + bs.fat_size_sectors * bs.number_of_fats) *
          bs.sector_size, SEEK_CUR);
    
    printf("Root dir_entries %d \n", bs.root_dir_entries);
    for(i=0; i<bs.root_dir_entries; i++) {
        fread(&entry, sizeof(entry), 1, in);
        print_file_info(&entry,i);
    }
    
    printf("\nLeido Root directory, ahora en 0x%X\n", ftell(in));
    fclose(in);
    return 0;
}
